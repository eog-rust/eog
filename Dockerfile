FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > eog.log'

COPY eog .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' eog
RUN bash ./docker.sh

RUN rm --force --recursive eog
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD eog
